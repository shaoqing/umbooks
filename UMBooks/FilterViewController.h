//
//  FilterViewController.h
//  UMBooks_Beta
//
//  Created by Shaoqing Zhu on 4/3/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#import "Datatypes.h"
#import "Parse/Parse.h"

@interface FilterViewController : UIViewController<CustomIOS7AlertViewDelegate,UITextViewDelegate,UITextFieldDelegate>

@property NSString* bookid;
@property Filter* myfilter;
- (id)initWithBookID:(NSNumber *)bookid;
-(void) AlertShow;

@end
