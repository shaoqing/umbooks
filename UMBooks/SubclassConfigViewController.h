//
//  SubclassConfigViewController.h
//  LogInAndSignUpDemo
//
//  Created by Yuchen Wen on 4/1/14.
//  Copyright 2014 Yuchen Wen All rights reserved.
//
#import <Availability.h>

#ifndef __IPHONE_3_0
#warning "This project uses features only available in iOS SDK 3.0 and later."
#endif

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "ICSPlainColorViewController.h"
#import "ProfileController.h"
#endif
@interface SubclassConfigViewController : ICSPlainColorViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UILabel *welcomeLabel;
@property(nonatomic, weak) ICSDrawerController *drawer;
- (IBAction)logOutButtonTapAction:(id)sender;

@end
