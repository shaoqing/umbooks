//
//  BookViewController.m
//  Colors
//
//  Created by Guan Wang on 3/19/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "BookViewController.h"
#import "GHContextMenuView.h"
#import "CourseViewController.h"

@interface BookViewController ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, GHContextOverlayViewDataSource, GHContextOverlayViewDelegate>

@end

@implementation BookViewController
@synthesize frontView;
@synthesize backView;
@synthesize FlipButton;
@synthesize goingToFrontView,drawer,mc;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.frontView = [[UIView alloc] initWithFrame:self.view.frame];
    //self.backView = [[UIView alloc] initWithFrame:self.view.frame];
    
    self.FlipButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.FlipButton.frame = CGRectMake(120, 360, 80, 80);
    UIImage *btnImage = [UIImage imageNamed:@"post.gif"];
    //UIImage *btnImage2 = [UIImage imageNamed:@"post_type_bubble_link@2x.png"];
    [self.FlipButton setBackgroundImage:btnImage forState:UIControlStateNormal];
    [self.FlipButton setBackgroundImage:btnImage forState:UIControlStateHighlighted];

 
    [self.FlipButton addTarget:self action:@selector(FlipButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ShowMail:)
                                                 name:@"Email"
                                               object:nil];
}

-(void) viewWillAppear:(BOOL)animated{
    backView.myid = frontView.myid;//check later
    frontView.drawer = self.drawer;
    self.goingToFrontView = NO;
    
    [self initFrontViewBackgroundColor];
    [self initBackViewBackGroundColor];
    [self addChildViewController:self.backView];
    [self addChildViewController:self.frontView];
    [self.view addSubview:self.backView.view];
    [self.view addSubview:self.frontView.view];
    [self.view setBackgroundColor:[UIColor whiteColor]];
  
    [[UIApplication sharedApplication].keyWindow addSubview:self.FlipButton];
  
    //[self.view addSubview:self.FlipButton];
}


-(void)viewWillDisappear:(BOOL)animated{
    [self.FlipButton removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)FlipButtonAction:(id)sender {
    self.goingToFrontView = !self.goingToFrontView;
    
    UIView *fromView = !self.goingToFrontView ? self.backView.view : self.frontView.view;
    UIView *toView = !self.goingToFrontView ? self.frontView.view : self.backView.view;
    if (self.goingToFrontView) {
        [backView loadObjects];
    }
    
    UIViewAnimationOptions transitionDirection = self.goingToFrontView ? UIViewAnimationOptionTransitionFlipFromRight : UIViewAnimationOptionTransitionFlipFromLeft;
    
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:1.0
                       options:transitionDirection
                    completion:^(BOOL finished) {
                        
                        [self showButtonTitle];
                        
                    }];
}

-(void) initFrontViewBackgroundColor{
}

-(void) initBackViewBackGroundColor{
    //self.backView.view.backgroundColor = [UIColor blueColor];
}

-(void) showButtonTitle{
    UIImage *btnImage = [UIImage imageNamed:@"post.gif"];
    UIImage *btnImage2 = [UIImage imageNamed:@"book.gif"];
    

    if(self.goingToFrontView){
        [self.FlipButton setBackgroundImage:btnImage2 forState:UIControlStateNormal];
        [self.FlipButton setBackgroundImage:btnImage2 forState:UIControlStateHighlighted];
    }
   else{
       [self.FlipButton setBackgroundImage:btnImage forState:UIControlStateNormal];
       [self.FlipButton setBackgroundImage:btnImage forState:UIControlStateHighlighted];
    }
}

- (void) ShowMail :(NSNotification *)notification {
    // Email Subject
    NSString *emailTitle = @"Interested In Your UMBooks Ad!";
    // Email Content
    NSString *messageBody = @"Hi, I am interested in your ad on UMBooks.";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:[notification object]];
    
    mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    [self addChildViewController:mc];
    // Present mail view controller on screen
    NSLog(@"BEFORE TRANSITION");
    UIView *fromView = self.backView.view;
    UIView *toView = self.mc.view;
    FlipButton.hidden = YES;
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:1.0
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    completion:^(BOOL finished) {
                        NSLog(@"To email");
                    }];
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    UIView *fromView = self.mc.view;
    UIView *toView = self.backView.view;
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:1.0
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    completion:^(BOOL finished) {
                        NSLog(@"To poster");
                    }];
    FlipButton.hidden = NO;
    [mc removeFromParentViewController];
    // Close the Mail Interface
    
}

@end
