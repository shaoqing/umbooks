//
//  Datatypes.m
//  UMBooks
//
//  Created by Shaoqing Zhu on 3/17/14.
//  Copyright (c) 2014 Shaoqing Zhu. All rights reserved.
//

#import "Datatypes.h"

@implementation Book
-(id)init {
    _author = @"Soloway";
    _bookinfo = @"Yahei";
    _title = @"441NB";    return self;
}
@end
@implementation Course
-(id)init {
    _courseDescrp = @"good";
    _courseName = @"Mobile app";
    _courseTitle = @"EECS441";
    return self;
}
@end

@implementation Poster
-(id)init{
    _posterid = @"temp";
    _isSell = [NSNumber numberWithInt:1];
    _price = [NSNumber numberWithInt:0];
    _note =@"nothing";
    return self;
}
@end
@implementation Filter
-(id)init{
    _sortOn = [NSNumber numberWithInt:0];
    _isDesc = [NSNumber numberWithInt:0];;
    _isSell = [NSNumber numberWithInt:0];
    _priceRange = NSMakeRange(0, INT32_MAX);
    return self;
}
@end
//To be added as above for the following three