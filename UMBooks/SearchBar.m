//
//  SearchBar.m
//  Colors
//
//  Created by Guan Wang on 3/19/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "SearchBar.h"

@implementation SearchBar
@synthesize searchBar,searchDc,table;

- (id)init{
    self = [super init];
    self.table = [[ICSColorsViewController alloc] initWithColors:nil ClassName:@""];
    return self;
}
- (void)loadView
{
    //在这里创建搜索栏和搜索显示控制器
    self.searchBar=[[UISearchBar  alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)];
    self.searchBar.tintColor=[UIColor colorWithRed:0.8f green:0.8f blue:0.8f alpha:1.0f];
    self.searchBar.autocorrectionType=UITextAutocorrectionTypeNo;
    self.searchBar.autocapitalizationType=UITextAutocapitalizationTypeNone;
    self.searchBar.keyboardType=UIKeyboardTypeAlphabet;
    self.searchBar.hidden=NO;
    self.searchBar.placeholder=[NSString stringWithCString:"请输入需要查找的文本内容"  encoding: NSUTF8StringEncoding];
    self.table.tableView.tableHeaderView=self.searchBar;
    
    self.searchDc=[[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.searchDc.searchResultsDataSource=self;
    self.searchDc.searchResultsDelegate=self;
    [self.searchDc  setActive:NO];
    
    
}
@end