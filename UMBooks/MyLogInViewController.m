//
//  MyLogInViewController.m
//  LogInAndSignUpDemo
//
//  Created by Yuchen Wen on 4/1/14.
//  Copyright 2014 Yuchen Wen All rights reserved.//

#import "MyLogInViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface MyLogInViewController ()
@property (nonatomic, strong) UIImageView *fieldsBackground;
@end

@implementation MyLogInViewController

@synthesize fieldsBackground;

- (void)viewDidLoad {
    [super viewDidLoad];
  [self.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UMBookslogo2.png"]]];
  NSArray *colors = @[[UIColor colorWithRed:237.0f/255.0f green:195.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:147.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:9.0f/255.0f blue:0.0f/255.0f alpha:1.0f]
                      ,
                      [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:153.0f/255.0f alpha:1.0f]
                      ];
  fieldsBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"backgroundfield.png"]];
  [self.fieldsBackground setFrame:CGRectMake(35.0f, 110.0f, 250.0f, 100.0f)];
  [self.logInView addSubview:self.fieldsBackground];
  [self.logInView sendSubviewToBack:self.fieldsBackground];
  [self.logInView setBackgroundColor:colors[3]];
 
   [self.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@"signup.png"] forState:UIControlStateNormal];
    [self.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@"signup.png"] forState:UIControlStateHighlighted];
     [self.logInView.logInButton setBackgroundImage:[UIImage imageNamed:@"login.png"] forState:UIControlStateNormal];
       [self.logInView.logInButton setBackgroundImage:[UIImage imageNamed:@"login.png"] forState:UIControlStateHighlighted];
  [self.logInView.passwordForgottenButton setBackgroundImage:[UIImage imageNamed:@"forgetpassword.png"] forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setBackgroundImage:[UIImage imageNamed:@"forgetpassword.png"] forState:UIControlStateHighlighted];
    [self.logInView.signUpLabel setShadowColor:colors[0]];
    [self.logInView.signUpLabel setTextColor:colors[1]];
    
  /*
   [self.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo.png"]]];
   //  [ self.logInView setLogo:<#(UIView *)#>]
   
   [self.logInView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"MainBG.png"]]];
   
   
   // Set buttons appearance
   [self.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@"Signup.png"] forState:UIControlStateNormal];
   [self.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@"SignupDown.png"] forState:UIControlStateHighlighted];
   [self.logInView.signUpButton setTitle:@"" forState:UIControlStateNormal];
   [self.logInView.signUpButton setTitle:@"" forState:UIControlStateHighlighted];
   
   // Add login field background
   fieldsBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LoginFieldBG.png"]];
   [self.logInView addSubview:self.fieldsBackground];
   [self.logInView sendSubviewToBack:self.fieldsBackground];
   
   // Remove text shadow
   CALayer *layer = self.logInView.usernameField.layer;
   layer.shadowOpacity = 0.0f;
   layer = self.logInView.passwordField.layer;
   layer.shadowOpacity = 0.0f;
   
   // Set field text color
   [self.logInView.usernameField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
   [self.logInView.passwordField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
   */
  CALayer *layer = self.logInView.usernameField.layer;
  layer.shadowOpacity = 0.0f;
  layer = self.logInView.passwordField.layer;
  layer.shadowOpacity = 0.0f;
  
  // Set field text color
  [self.logInView.usernameField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
  [self.logInView.passwordField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
  
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
  [self.logInView.logInButton setTitle:@"" forState:UIControlStateNormal];
    [self.logInView.logInButton setTitle:@"" forState:UIControlStateHighlighted];
    [self.logInView.signUpButton setTitle:@"" forState:UIControlStateNormal];
   [self.logInView.signUpButton setTitle:@"" forState:UIControlStateHighlighted];
   [self.logInView.passwordForgottenButton setTitle:@"" forState:UIControlStateNormal];
   [self.logInView.passwordForgottenButton setTitle:@"" forState:UIControlStateHighlighted];
    [self.logInView.logo setFrame:CGRectMake(120.0f, 30.0f, 80.0f, 80.0f)];
  [self.logInView.passwordForgottenButton setFrame:CGRectMake(10.0f, 125.0f, 20.0f, 60.0f)];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
