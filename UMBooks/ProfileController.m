//
//  ProfileController.m
//  UMBooks_Beta
//
//  Created by Wen Yuchen on 4/9/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "ProfileController.h"

@implementation ProfileController
@synthesize closeButton,flag,ltoP,myid,topView,button1;
- (void)viewDidLoad
{
  [super viewDidLoad];
  NSLog(@"chata!!");
  // Initialize and add the openDrawerButton
  
  if ([PFUser currentUser])
  {
    PFUser *user = [PFUser currentUser];
    
    //  PFQuery *query = [PFUser query];
    //  [query whereKey:@"username" equalTo:user.username]; // find all the women
    // NSArray *result = [query findObjects];
    UILabel* name3label = [[UILabel alloc]initWithFrame:CGRectMake(90, 25, 90, 30)];
    name3label.text = @"Profile";
    [name3label setFont:[UIFont systemFontOfSize:30]];
    UILabel* namelabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 60, 90, 30)];
    namelabel.text = @"Nick Name:";
    UILabel* name2label = [[UILabel alloc]initWithFrame:CGRectMake(120, 60, 90, 30)];
    name2label.text = [user objectForKey:@"additional"];
    UILabel* posterlabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 90, 90, 30)];
    posterlabel.text = @"Poster:";
   // UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
      _myprofileviewcontroller = [[ProfileTableViewController alloc]initWithUsername:user.username];
        _myprofileviewcontroller.view.frame = CGRectMake(30, 120, 250,300);
    [_myprofileviewcontroller.tableView setEditing:NO animated:YES];
    self.button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button1 setTitle:@"" forState:UIControlStateNormal];
  //    [button setTitle:@"" forState:UIControlStateHighlighted];
    button1.frame = CGRectMake(230, 90, 50, 30);
    [button1 setBackgroundImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
    //   [button setBackgroundImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateHighlighted];
    [button1 addTarget:self action:@selector(stayPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *logoutButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [logoutButton setTitle:@"" forState:UIControlStateNormal];
    logoutButton.frame = CGRectMake(30, 420, 250, 40);
       [logoutButton setBackgroundImage:[UIImage imageNamed:@"logout.png"] forState:UIControlStateNormal];
    [logoutButton addTarget:self action:@selector(logoutListener:) forControlEvents:UIControlEventTouchUpInside];
      
      closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
      [closeButton setTitle:@"" forState:UIControlStateNormal];
     [closeButton setBackgroundImage:[UIImage imageNamed:@"close_icon.png"] forState:UIControlStateNormal];
      closeButton.frame = CGRectMake(260, 20, 40, 40);
      //UIImage* closeicon = [UIImage imageNamed:@"close_icon.png"];
      //[closeButton setImage:closeicon forState:UIControlStateNormal];
      [closeButton addTarget:self action:@selector(goBackView:) forControlEvents:UIControlEventTouchDown];
      
      
      UIButton* openDrawerButtonr = [UIButton buttonWithType:UIButtonTypeRoundedRect];
      openDrawerButtonr.frame = CGRectMake(285, 270, 25, 25);
      [openDrawerButtonr setBackgroundImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [openDrawerButtonr addTarget:self action:@selector(openDrawerr:) forControlEvents:UIControlEventTouchUpInside];
      
     
    [self.view addSubview:button1];
    [self.view addSubview:name3label];
    [self.view addSubview:namelabel];
    [self.view addSubview:name2label];
    [self.view addSubview:posterlabel];
    [self.view addSubview:_myprofileviewcontroller.view];
    [self.view addSubview:logoutButton];
    [self.view addSubview:openDrawerButtonr];
    if (ltoP != 1) {
      [self.view addSubview:closeButton];
    }
    
  }
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"p"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"p"];
       [self showTutorialOverlay];
    }
}
-(id)initWithLoginToProfile:(int)ltoP {
  self = [super init];
  self.ltoP = ltoP;
  return self;
}
-(void)stayPressed:(UIButton *) sender {
  if (sender.selected == YES) {
    sender.selected = NO;
    [button1 setBackgroundImage:[UIImage imageNamed:@"Edit.png"] forState:UIControlStateNormal];

          [_myprofileviewcontroller.tableView setEditing:NO animated:YES];
  }else{
    sender.selected = YES;
    [button1 setBackgroundImage:[UIImage imageNamed:@"Done.png"] forState:UIControlStateNormal];

    [_myprofileviewcontroller.tableView setEditing:YES animated:YES];
  }
}
- (void)openDrawerr:(id)sender
{
    [self.drawer openr];
    
}
-(void)goBackView:(UIButton *) sender {
    if (flag==0) {
        PFQuery* query = [[PFQuery alloc]initWithClassName:@"Courses"];
        [query whereKey:@"courseID" equalTo:myid];
        NSLog(@"Go back to Course: %@",myid);
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            NSString* courseinfo = [object objectForKey:@"courseDecrp"];
            NSString* name = [object objectForKey:@"courseName"];
            NSString* title = [object objectForKey:@"courseTitle"];
            CourseViewController *center = [[CourseViewController alloc] initWithCourseName:name Title:title Description:courseinfo];
            center.myid = myid;
            center.view.backgroundColor = _clr;
            center.flag = 0;
            [self.drawer replaceCenterViewControllerWithViewController:center];
            NSArray *colorsr = @[[UIColor colorWithRed:0.0f/255.0f green:250.0f/255.0f blue:154.0f/255.0f alpha:1.0f],
                                 [UIColor colorWithRed:100.0f/255.0f green:149.0f/255.0f blue:237.0f/255.0f alpha:1.0f],
                                 [UIColor colorWithRed:255.0f/255.0f green:105.0f/255.0f blue:180.0f/255.0f alpha:1.0f]
                                 ];
            
            BookTableViewController *right = [[BookTableViewController alloc] initWithColors:colorsr CourseID:myid];
            [self.drawer replaceRightViewControllerWithViewController:right];
        }];
    }
    else if (flag == 1) {
        PFQuery* query = [[PFQuery alloc]initWithClassName:@"Books"];
        [query whereKey:@"bookID" equalTo:myid];
        NSLog(@"Go back to Book: %@",myid);
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            NSString* bookinfo = [object objectForKey:@"bookInfo"];
            NSString* author = [object objectForKey:@"Author"];
            NSString* title = [object objectForKey:@"Title"];
            NSString* courseid = [object objectForKey:@"courseID"];
            CourseViewController *center = [[CourseViewController alloc] initWithCourseName:title Title:author Description:bookinfo];
            center.myid = myid;
            center.flag = 1;
            center.view.backgroundColor = _clr;
            
            PosterTableViewController *back = [[PosterTableViewController alloc] initWithBookid:myid withFilter:NULL];
            back.view.frame = CGRectMake(0, 50, 320, 590);
            BookViewController *book = [[BookViewController alloc] init];
            [book setFrontView:center];
            [book setBackView:back];
            [self.drawer replaceCenterViewControllerWithViewControllerr:book];
            NSArray *colorsr = @[[UIColor colorWithRed:0.0f/255.0f green:250.0f/255.0f blue:154.0f/255.0f alpha:1.0f],
                                 [UIColor colorWithRed:100.0f/255.0f green:149.0f/255.0f blue:237.0f/255.0f alpha:1.0f],
                                 [UIColor colorWithRed:255.0f/255.0f green:105.0f/255.0f blue:180.0f/255.0f alpha:1.0f]
                                 ];
 
            BookTableViewController *right = [[BookTableViewController alloc] initWithColors:colorsr CourseID:courseid];
            [self.drawer replaceRightViewControllerWithViewController:right];
            
            
        }];
    }
    
}

-(void)logoutListener:(UIButton *) sender {
  [PFUser logOut];
  SubclassConfigViewController *plainColorVC = [[SubclassConfigViewController alloc] init];
  NSArray *colors = @[[UIColor colorWithRed:237.0f/255.0f green:195.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:147.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:9.0f/255.0f blue:0.0f/255.0f alpha:1.0f]
                      ];
  plainColorVC.view.backgroundColor = colors[0];
  [self.drawer replaceCenterViewControllerWithViewControllerr:plainColorVC];
   

}

-(void) showTutorialOverlay
{
    topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    
    UIView *tutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [tutView setBackgroundColor:[UIColor blackColor]];
    [tutView setAlpha:0.4];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = topView.frame;
    [button addTarget:self action:@selector(hideTutorialOverlay) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
    
    UIImageView *tutImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oprofile.gif"]];
    [tutImageView setFrame:CGRectMake(0, (-1) * statusBarFrame.size.height, 320, 480)];
    
    
    [tutView addSubview:button];
    [topView addSubview:tutView];
    [topView addSubview:tutImageView];
    
    [self.view addSubview:topView];
    
}

-(void)hideTutorialOverlay{
    [topView removeFromSuperview];
}
@end
