//
//  SearchBar.h
//  Colors
//
//  Created by Guan Wang on 3/19/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICSColorsViewController.h"

@interface SearchBar : UIViewController


@property UISearchBar  *  searchBar;
@property UISearchDisplayController *  searchDc;
@property ICSColorsViewController* table;

@end