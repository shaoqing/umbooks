//
//  PosterAddingViewController.m
//  Colors
//
//  Created by Shaoqing Zhu on 3/27/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "PosterAddingViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface PosterAddingViewController ()
@end
@implementation PosterAddingViewController
@synthesize poster,namelabel,notecontent,price,isEdit,objid;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
- (id)initWithBookID:(NSNumber *)bookid {
    poster = [[Poster alloc]init];
    PFUser* myuser = [PFUser currentUser];
    poster.username = myuser.username;
    poster.bookid = bookid;
    poster.nickname = [myuser objectForKey:@"additional"];
    poster.email = [myuser objectForKey:@"email"];
    
    return self;
}
- (id) init {
    poster = [[Poster alloc]init];
    PFUser* myuser = [PFUser currentUser];
    poster.username = myuser.username;
    poster.nickname = [myuser objectForKey:@"additional"];
    poster.email = [myuser objectForKey:@"email"];
    return self;
}
-(void) AlertShow {
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self addposterView]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK", @"Cancel", nil]];
    [alertView setDelegate:self];
    
    NSLog(@"Alert View Ready");
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
        if (buttonIndex == 0) {
            UIView* content = alertView.containerView;
            UITextField* pricefield = [content viewWithTag:1];
            UISegmentedControl* typecontrol = [content viewWithTag:2];
            UITextView* notevw = [content viewWithTag:3];
            
            NSNumberFormatter* f = [[NSNumberFormatter alloc]init];
            poster.price = [f numberFromString:pricefield.text];
            poster.note = notevw.text;
            if ([typecontrol selectedSegmentIndex] == 0) {
                poster.isSell = [NSNumber numberWithInt:-1];
            }
            
            NSLog(@"poster, price: %@, note: %@, isell: %@",poster.price,poster.note,poster.isSell);
            if (!poster.price) {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
            }
            else {
                if (isEdit) {
                    [self updateToParce:poster];
                }
                else {
                    [self saveToParce:poster];
                }
                if (_isPT) {
                    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification                                                                notificationWithName:@"Poster Added"                                                                object:Nil]];
                }
                [alertView close];
            }

        }
        else if (buttonIndex == 1) {
            [alertView close];
        }

        
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
    [alertView close];
}
-(void) saveToParce: (Poster*) pos {
    NSLog(@"AdP");
    
    PFObject* posterObj = [PFObject objectWithClassName:@"Posters"];
    [posterObj setObject:pos.isSell forKey:@"isSell"];
    [posterObj setObject:pos.price forKey:@"price"];
    [posterObj setObject:pos.note forKey:@"note"];
    [posterObj setObject:pos.nickname forKey:@"nickname"];
    
    [posterObj setObject:pos.username forKey:@"userName"];
    [posterObj setObject:pos.email forKey:@"Email"];
    
    [posterObj setObject:pos.bookid forKey:@"bookID"];
    NSLog(@"BOOKID: %@",pos.bookid);
    [posterObj saveInBackground];
}

-(void) updateToParce: (Poster*) pos {
    // Create a pointer to an object of class Point with id dlkj83d
    
    PFObject* posterObj = [PFObject objectWithoutDataWithClassName:@"Posters" objectId:objid];
    [posterObj setObject:pos.isSell forKey:@"isSell"];
    [posterObj setObject:pos.price forKey:@"price"];
    [posterObj setObject:pos.note forKey:@"note"];
    
    [posterObj saveInBackground];
}

- (UIView *)addposterView
{
    UIView *posterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 350)];
    
    
    namelabel.text = poster.nickname;
    UILabel* pricelabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 80, 90, 30)];
    pricelabel.text = @"Set Price: ";
    UILabel* typelabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 130, 90, 30)];
    typelabel.text = @"Sell or Buy?";
    UILabel* notelabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 180, 90, 30)];
    notelabel.text = @"Leave a note: ";
        
    namelabel = [[UITextField alloc]initWithFrame:CGRectMake(10, 30, 270, 30)];
    namelabel.text =poster.nickname;
    namelabel.enabled  = NO;
    [namelabel setAutocorrectionType:UITextAutocorrectionTypeNo];
    [namelabel setBackgroundColor:[UIColor whiteColor]];
    [namelabel setReturnKeyType:UIReturnKeyNext];
    [namelabel setBorderStyle:UITextBorderStyleRoundedRect];
    [namelabel setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    price = [[UITextField alloc]initWithFrame:CGRectMake(10, 80, 270, 30)];
    price.tag = 1;
    price.delegate = self;
    [price setBackgroundColor:[UIColor whiteColor]];
    price.placeholder = @"Set Price";
    [price setAutocorrectionType:UITextAutocorrectionTypeNo];
    [price setBackgroundColor:[UIColor whiteColor]];
    [price setReturnKeyType:UIReturnKeyNext];
    [price setBorderStyle:UITextBorderStyleRoundedRect];
    [price setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    UISegmentedControl* type = [[UISegmentedControl alloc]initWithFrame:CGRectMake(10, 130, 270, 30)];
    [type insertSegmentWithTitle:@"BUY" atIndex:0 animated:YES];
    [type insertSegmentWithTitle:@"SELL" atIndex:1 animated:YES];
    type.tag = 2;

    notecontent = [[GCPlaceholderTextView alloc]initWithFrame:CGRectMake(10, 180, 270, 150)];
    notecontent.tag = 3;
    notecontent.delegate = self;
    notecontent.placeholder = @"Leave a Note";
    
    notecontent.placeholderColor = [UIColor colorWithRed:200/255.0f green:200/255.0f blue:200/255.0f alpha:1.0f];
    notecontent.font = [UIFont systemFontOfSize:17];
    [notecontent setAutocorrectionType:UITextAutocorrectionTypeNo];
    [notecontent setBackgroundColor:[UIColor whiteColor]];
    [notecontent setReturnKeyType:UIReturnKeyDone];
    //To make the border look very close to a UITextField
    [notecontent.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [notecontent.layer setBorderWidth:0.5];
    //The rounded corner part, where you specify your view's corner radius:
    notecontent.layer.cornerRadius = 5;
    notecontent.clipsToBounds = YES;
    [notecontent setTextAlignment:UITextAlignmentLeft];
    
    if (isEdit) {
        PFQuery* query = [[PFQuery alloc]initWithClassName:@"Posters"];
                NSLog(@"Editing Poster: %@",objid);
        [query getObjectInBackgroundWithId:objid block:^(PFObject *object, NSError *error) {
            NSNumber* numprice = [object objectForKey:@"price"];
            price.text = [numprice stringValue];
            notecontent.text = [object objectForKey:@"note"];
            objid = object.objectId;
            NSNumber* issell = [object objectForKey:@"isSell"];
            if ([issell isEqualToNumber:[NSNumber numberWithInt:1]]) {
                type.selectedSegmentIndex = 1;
            }
            else {
                type.selectedSegmentIndex = 0;
            }
            
        }];
        NSLog(@"POSTER EDITING VIEW SHOULD APPEAR");
    }
    
    [posterView addSubview:namelabel];
    [posterView addSubview:price];
    [posterView addSubview:type];
    [posterView addSubview:notecontent];
    return posterView;
}
-(BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    BOOL ret = NO;
    if(textField == self.price) {
        [self.notecontent becomeFirstResponder];
    }   else {
        ret = YES;
        [textField resignFirstResponder];
    }
    return ret;
}
@end