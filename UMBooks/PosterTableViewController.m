//
//  PosterTableViewController.m
//  UMBooks
//
//  Created by Shaoqing Zhu on 3/18/14.
//  Copyright (c) 2014 Shaoqing Zhu. All rights reserved.
//

#import "PosterTableViewController.h"
#import "CHTumblrMenuView.h"
#import "GHContextMenuView.h"
#import "ProfileController.h"

@interface PosterTableViewController ()

@end

@implementation PosterTableViewController
@synthesize myfilter,myid,isUser,aButton,daButton,topView;
-(id) initWithBookid:(NSNumber *)bookid withFilter:(Filter*)filter

{
    self = [super initWithClassName:@"PosterTable"];
    
    if (self) {
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"Posters";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"note";
        
        // The title for this table in the Navigation Controller.
        self.title = @"Poster";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 10;
    }
    _CurrentObj = [[NSMutableArray alloc] init];
    myid = bookid;
    myfilter= filter;
    
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CALayer *layer = self.tableView.layer;
    [layer setMasksToBounds:YES];
    [layer setCornerRadius: 6.0];
    [layer setBorderWidth:1.5];
    [layer setBorderColor:[[UIColor colorWithWhite: 0.8 alpha: 1.0] CGColor]];
    
    //Observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RefreshPosterTable)
                                                 name:@"Poster Added"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(FilterPoster:)
                                                 name:@"Filter"
                                               object:nil];

    
    self.aButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.aButton.frame = CGRectMake(200, 310, 80, 80);
    UIImage *btnImage = [UIImage imageNamed:@"filter.gif"];
    //UIImage *btnImage2 = [UIImage imageNamed:@"post_type_bubble_link@2x.png"];
    [self.aButton setBackgroundImage:btnImage forState:UIControlStateNormal];
    [self.aButton setBackgroundImage:btnImage forState:UIControlStateHighlighted];
    
    
    [self.aButton addTarget:self action:@selector(AddFilter) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.aButton];
    
    self.daButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.daButton.frame = CGRectMake(40, 310, 80, 80);
    UIImage *btnImagez = [UIImage imageNamed:@"addposter.gif"];
    //UIImage *btnImage2 = [UIImage imageNamed:@"post_type_bubble_link@2x.png"];
    [self.daButton setBackgroundImage:btnImagez forState:UIControlStateNormal];
    [self.daButton setBackgroundImage:btnImagez forState:UIControlStateHighlighted];
    
    
    [self.daButton addTarget:self action:@selector(AddPoster) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.daButton];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"po"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"po"];
        [self showTutorialOverlay];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    _CurrentObj = Nil;//empty local memo
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if ([PFUser currentUser]) {
        [self loadObjects];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Parse

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    // This method is called every time objects are loaded from Parse via the PFQuery
}

- (void)objectsWillLoad {
    [super objectsWillLoad];
    
    // This method is called before a PFQuery is fired to get more objects
}


// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    [_CurrentObj removeAllObjects];
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    NSLog(@"QUERY FOR POSTER");
    
    query.cachePolicy = kPFCachePolicyIgnoreCache;
    
    //not user's poster
    if (myfilter == NULL) {
        [query whereKey:@"bookID" equalTo:myid];
        [query orderByDescending:@"createdAt"];
        NSLog(@"poster belong to %@,",myid);
    }
    else {
        [query whereKey:@"bookID" equalTo:myid];
        if ([myfilter.sortOn isEqualToNumber:[NSNumber numberWithInt:1]]) {
            NSLog(@"QUERY:  Time");
            if([myfilter.isDesc isEqualToNumber:[NSNumber numberWithInt:1]]){
                NSLog(@"QUERY: Desc ");
                [query orderByDescending:@"createdAt"];
            }
            else if ([myfilter.isDesc isEqualToNumber:[NSNumber numberWithInt:-1]]) {
                NSLog(@"QUERY: Ascd ");
                [query orderByAscending:@"createdAt"];
            }
        }
        else if ([myfilter.sortOn isEqualToNumber:[NSNumber numberWithInt:-1]]){
            NSLog(@"QUERY:  Price");
            if([myfilter.isDesc isEqualToNumber:[NSNumber numberWithInt:1]]){
                NSLog(@"QUERY: Desc ");
                [query orderByDescending:@"price"];
            }
            else if ([myfilter.isDesc isEqualToNumber:[NSNumber numberWithInt:-1]]) {
                NSLog(@"QUERY: Ascd ");
                [query orderByAscending:@"price"];
            }
        }
        
        if ([myfilter.isSell isEqualToNumber:[NSNumber numberWithInt:1]]) {
            NSLog(@"QUERY: Sell");
            [query whereKey:@"isSell" equalTo:[NSNumber numberWithInt:1]];
        }
        else if ([myfilter.isSell isEqualToNumber:[NSNumber numberWithInt:-1]]){
            NSLog(@"QUERY: Buy");
            [query whereKey:@"isSell" equalTo:[NSNumber numberWithInt:-1]];
        }
        /*[query whereKey:@"price" greaterThanOrEqualTo:[NSNumber numberWithInteger:myfilter.priceRange.location]];
        [query whereKey:@"price" lessThanOrEqualTo:[NSNumber numberWithInteger:myfilter.priceRange.length]];*/
        
    }
    return query;
}



// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the first key in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell
    cell.textLabel.text = [NSString stringWithFormat:@"$%@",[object objectForKey:@"price"]]; //set text
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [object objectForKey:@"nickname"]]; //set small text
    NSNumber *issell = [object objectForKey:@"isSell"];
    NSLog(@"sell? %@",issell);
    
    if ([issell isEqualToNumber:[NSNumber numberWithInt:1]]) {
        [cell setBackgroundColor: [UIColor colorWithRed:135.0f/255.0f green:206.0f/255.0f blue:235.0f/255.0f alpha:1.0f]]; //set cell color
    }
    else {
        [cell setBackgroundColor: [UIColor colorWithRed:152.0f/255.0f green:251.0f/255.0f blue:152.0f/255.0f alpha:1.0f]];
    }
    
    [_CurrentObj addObject:object];
    NSLog(@"array size is: %i, OBjid is %@",_CurrentObj.count,[[_CurrentObj lastObject] objectId]);
    
    return cell;
}


/*
 // Override if you need to change the ordering of objects in the table.
 - (PFObject *)objectAtIndex:(NSIndexPath *)indexPath {
 return [objects objectAtIndex:indexPath.row];
 }
 */

/*
 // Override to customize the look of the cell that allows the user to load the next page of objects.
 // The default implementation is a UITableViewCellStyleDefault cell with simple labels.
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
 static NSString *CellIdentifier = @"NextPage";
 
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 
 if (cell == nil) {
 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
 }
 
 cell.selectionStyle = UITableViewCellSelectionStyleNone;
 cell.textLabel.text = @"Load more...";
 
 return cell;
 }
 */

#pragma mark - Table view data source

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    NSLog(@"Select %lu",(long)indexPath.row);
    PFObject* obj = _CurrentObj[indexPath.row];
    NSLog(@"OBjid is %@",obj.objectId);
    PFUser* myuser = [PFUser currentUser];
    NSString* name = [obj objectForKey:@"userName"];
    NSLog(@"myname: %@, name: %@",myuser.username,name);
    if ([myuser.username isEqualToString:name]) {
        [self EditPoster:obj.objectId];
    }
    else {
        _posterdetail = [[PosterDetailView alloc]init];
        _posterdetail.email = [obj objectForKey:@"Email"];
        _posterdetail.note = [obj objectForKey:@"note"];
        [_posterdetail AlertShow];
    }
    
}

- (void)FlipButtonAction:(id)sender {

        [self showMenuÏ];

}

- (void)showMenuÏ
{
    CHTumblrMenuView *menuView = [[CHTumblrMenuView alloc] init];
    [menuView addMenuItemWithTitle:@"Add Poster" andIcon:[UIImage imageNamed:@"addposter.gif"] andSelectedBlock:^{
        [self AddPoster];
        NSLog(@"Quote selected");
        
    }];

        [menuView addMenuItemWithTitle:@"Filter" andIcon:[UIImage imageNamed:@"filter.gif"] andSelectedBlock:^{
        [self AddFilter];
        NSLog(@"Video selected");
        
    }];
    
    
    
    [menuView show];
}




-(void) AddPoster {
    _posteraddingview = [[PosterAddingViewController alloc] initWithBookID:myid];
    _posteraddingview.isPT = YES;
    _posteraddingview.isEdit = FALSE;
    [_posteraddingview AlertShow];
}

-(void) EditPoster:(NSString*) objid {
    _posteraddingview = [[PosterAddingViewController alloc] initWithBookID:myid];
    _posteraddingview.isPT = YES;
    _posteraddingview.isEdit = YES;
    _posteraddingview.objid = objid;
    [_posteraddingview AlertShow];
}


-(void) AddFilter {
    _filterview = [[FilterViewController alloc] initWithBookID:myid];
    [_filterview AlertShow];
}

-(void) RefreshPosterTable {
    [self loadObjects];
    NSLog(@"Poster Refreshed");
}

-(void) FilterPoster:(NSNotification *)notification {
    Filter *filter = [notification object];
    myfilter = filter;
    [self loadObjects];
    NSLog(@"MY FILTER: %@, %@, %@",myfilter.sortOn,myfilter.isSell,myfilter.isDesc);
    NSLog(@"Filter Applied");
}

-(void) showTutorialOverlay
{
    topView = [[UIView alloc] initWithFrame:CGRectMake(0, -50, 320, 480)];
    
    UIView *tutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [tutView setBackgroundColor:[UIColor blackColor]];
    [tutView setAlpha:0.4];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = topView.frame;
    [button addTarget:self action:@selector(hideTutorialOverlay) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
    
    UIImageView *tutImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oposter.gif"]];
    [tutImageView setFrame:CGRectMake(0, (-1) * statusBarFrame.size.height, 320, 480)];
    
    
    [tutView addSubview:button];
    [topView addSubview:tutView];
    [topView addSubview:tutImageView];
    
    [self.view addSubview:topView];
    
}

-(void)hideTutorialOverlay{
    [topView removeFromSuperview];
}
@end