//
//  main.m
//  UMBooks
//
//  Created by Shaoqing Zhu on 3/16/14.
//  Copyright (c) 2014 Shaoqing Zhu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ICSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ICSAppDelegate class]));
    }
}
