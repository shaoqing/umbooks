//
//  PosterDetailView.h
//  UMBooks_Beta
//
//  Created by Shaoqing Zhu on 4/11/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#import "Datatypes.h"
#import "Parse/Parse.h"
#import <MessageUI/MessageUI.h>


@interface PosterDetailView : UIViewController<CustomIOS7AlertViewDelegate,MFMailComposeViewControllerDelegate>
@property NSString* note;
@property NSString* email;
-(void) AlertShow;

@end
