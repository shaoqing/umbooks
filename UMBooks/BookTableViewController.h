//
//  BookTableViewController.h
//  Colors
//
//  Created by Guan Wang on 3/19/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "ICSDrawerController.h"

@interface BookTableViewController : PFQueryTableViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting>

@property(nonatomic, weak) ICSDrawerController *drawer;
@property  NSMutableArray* CurrentObj;
@property BOOL isMylist;
@property NSNumber* myid;//courseid of the course this books belong to

- (id)initWithColors:(NSArray *)colors CourseID:(NSNumber *)courseid;
- (id)initWithColors:(NSArray *)colors;

@end
