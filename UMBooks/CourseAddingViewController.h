//
//  CourseAddingViewController.h
//  UMBooks_Beta
//
//  Created by Shaoqing Zhu on 3/27/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#import "Datatypes.h"
#import "Parse/Parse.h"
#import "GCPlaceholderTextView.h"


@interface CourseAddingViewController : UIViewController<CustomIOS7AlertViewDelegate,UITextViewDelegate,UITextFieldDelegate>

@property Course* course;
@property UITextField* coursetitle;
@property UITextField* name ;
@property GCPlaceholderTextView * courseinfo;
@property NSString* objid;
@property BOOL isEdit;
@property NSNumber* myid;

- (void) AlertShow;
@end
