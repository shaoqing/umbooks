//
//  CourseAddingViewController.m
//  UMBooks_Beta
//
//  Created by Shaoqing Zhu on 3/27/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "CourseAddingViewController.h"
#import <QuartzCore/QuartzCore.h>
@interface CourseAddingViewController ()

@end

@implementation CourseAddingViewController


@synthesize course,courseinfo,coursetitle,name,objid,isEdit,myid;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void) AlertShow {
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
    alertView.alpha = 1;
    
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self addcourseView]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK", @"Cancel", nil]];
    [alertView setDelegate:self];
    
    NSLog(@"Alert View Ready");
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
        if (buttonIndex == 0) {
            UIView* content = alertView.containerView;
            UITextField* title = [content viewWithTag:1];
            UITextField* name = [content viewWithTag:2];
            UITextView* info = [content viewWithTag:3];
            
            course = [[Course alloc]init];
            course.courseTitle = title.text;
            course.courseName = name.text;
            course.courseDescrp = info.text;
            NSLog(@"%@,%@,%@",course.courseTitle,course.courseName,course.courseDescrp);
            if ([course.courseDescrp  isEqual: @""]&&[course.courseName isEqual:@""]&&[course.courseTitle isEqual:@""]) {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
            }
            else {                
                if (isEdit) {
                    [self updateToParce:course];
                    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification                                                                notificationWithName:@"Course Edited"                                                                object:course]];
                }
                else {
                    [self saveToParce:course];
                }
                NSLog(@"ALertview close");
                [alertView close];
            }
        }
        else if (buttonIndex == 1) {
            NSLog(@"ALertview close");
            [alertView close];
        }

        
    }];
    
    [alertView setUseMotionEffects:true];
    NSLog(@"Alert will show");
    // And launch the dialog
    [alertView show];
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
    [alertView close];
}
-(void) saveToParce: (Course*) cs {
    // Create the Courses Table
    PFObject* courseObj = [PFObject objectWithClassName:@"Courses"];
    NSLog(@"AdC");
    PFQuery *qry = [[PFQuery alloc]initWithClassName:@"IDs"];
    [qry getObjectInBackgroundWithId:@"y0W9uVdheP" block:^(PFObject *object, NSError *error) {
        NSNumber* currentid=[object objectForKey:@"currentID"];
        [courseObj setObject:currentid forKey:@"courseID"];
        [courseObj setObject:cs.courseName forKey:@"courseName"];
        [courseObj setObject:cs.courseTitle forKey:@"courseTitle"];
        [courseObj setObject:cs.courseDescrp forKey:@"courseDecrp"];
        
        [courseObj saveInBackground];
    }];
    
    PFObject *idobj = [PFObject objectWithoutDataWithClassName:@"IDs" objectId:@"y0W9uVdheP"];    
    [idobj incrementKey:@"currentID"];
    [idobj saveInBackground];
    // Create the Courses Table
    
}

-(void) updateToParce: (Course*) cs {
    // Create a pointer to an object of class Point with id dlkj83d
    PFObject *courseObj = [PFObject objectWithoutDataWithClassName:@"Courses" objectId:objid];
    [courseObj setObject:cs.courseName forKey:@"courseName"];
    [courseObj setObject:cs.courseTitle forKey:@"courseTitle"];
    [courseObj setObject:cs.courseDescrp forKey:@"courseDecrp"];
    [courseObj saveInBackground];
    
}

- (UIView *)addcourseView
{
    UIView *courseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 350)];
    coursetitle = [[UITextField alloc]initWithFrame:CGRectMake(10, 30, 270, 30)];
    coursetitle.placeholder = @"Course Number";
    coursetitle.tag = 1;
    coursetitle.delegate = self;
    [coursetitle setAutocorrectionType:UITextAutocorrectionTypeNo];
    [coursetitle setBackgroundColor:[UIColor whiteColor]];
    [coursetitle setReturnKeyType:UIReturnKeyNext];
    [coursetitle setBorderStyle:UITextBorderStyleRoundedRect];
    [coursetitle setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    name = [[UITextField alloc]initWithFrame:CGRectMake(10, 80, 270, 30)];
    name.placeholder = @"Course Name ";
    name.tag = 2;
    name.delegate = self;
    [name setAutocorrectionType:UITextAutocorrectionTypeNo];
    [name setBackgroundColor:[UIColor whiteColor]];
    [name setReturnKeyType:UIReturnKeyNext];
    [name setBorderStyle:UITextBorderStyleRoundedRect];
    [name setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    
    
    courseinfo = [[GCPlaceholderTextView alloc]initWithFrame:CGRectMake(10, 130, 270, 180)];
    courseinfo.placeholder = @"Course Description";
    courseinfo.placeholderColor = [UIColor colorWithRed:200/255.0f green:200/255.0f blue:200/255.0f alpha:1.0f];
    courseinfo.font = [UIFont systemFontOfSize:17];
    courseinfo.tag = 3;
    courseinfo.delegate = self;
    [courseinfo setAutocorrectionType:UITextAutocorrectionTypeNo];
    [courseinfo setBackgroundColor:[UIColor whiteColor]];
    [courseinfo setReturnKeyType:UIReturnKeyDone];
    //To make the border look very close to a UITextField
    [courseinfo.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [courseinfo.layer setBorderWidth:0.5];
    //The rounded corner part, where you specify your view's corner radius:
    courseinfo.layer.cornerRadius = 5;
    courseinfo.clipsToBounds = YES;
    [courseinfo setTextAlignment:UITextAlignmentLeft];
    //[courseinfo setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    
    if (isEdit) {
        PFQuery* query = [[PFQuery alloc]initWithClassName:@"Courses"];
        [query whereKey:@"courseID" equalTo:myid];
        NSLog(@"Editing Course: %@",myid);
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            coursetitle.text = [object objectForKey:@"courseTitle"];
            courseinfo.text = [object objectForKey:@"courseDecrp"];
            name.text = [object objectForKey:@"courseName"];
            objid = object.objectId;
            
        }];
        
    }
    
    
    [courseView addSubview:coursetitle];
    [courseView addSubview:name];
    [courseView addSubview:courseinfo];
    return courseView;
}
-(BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    BOOL ret = NO;
    if(textField == self.coursetitle) {
        [self.name becomeFirstResponder];
    } else if (textField == self.name) {
        [self.courseinfo becomeFirstResponder  ];
    } else {
        ret = YES;
        [textField resignFirstResponder];
    }
    
    return ret;
}
@end
