//
//  BookAddingViewController.h
//  Colors
//
//  Created by Shaoqing Zhu on 3/27/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#import "Datatypes.h"
#import "Parse/Parse.h"
#import "GCPlaceholderTextView.h"

@interface BookAddingViewController : UIViewController<CustomIOS7AlertViewDelegate,UITextViewDelegate,UITextFieldDelegate>

@property Book* book;
@property UITextField* bookname;
@property UITextField* author ;
//@property UITextField* isbn;
@property GCPlaceholderTextView* bookinfo;
@property NSString* objid;
@property BOOL isEdit;
@property NSNumber* myid;

- (id)initWithCourseID:(NSNumber *)courseid;
- (void) AlertShow;
@end